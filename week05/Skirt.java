package week05;
public class Skirt extends Cloth {
    public Skirt(int quantity, String brand, String color, double taxRate, String name) {
        super(taxRate, color, brand);
        this.setQuantity(quantity);
        this.setName(name);
    }

    public String toString() {
       System.out.println( "-------------------------------"+"\nPurchased Cloth Items");
       System.out.println(); 
       return "Name: " + super.getName() + "\n" +
                super.toString();
    }
}
