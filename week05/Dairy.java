package week05;
public class Dairy extends Food {

    private boolean pasteurized;
    private double price, rate, kgs, total;

    public Dairy(double taxRate, double kgs, boolean pasteurized, String name) {
        super(taxRate, kgs);
        this.pasteurized = pasteurized;
        this.setName(name);
    }

    public boolean isPasteurized() {
        return this.pasteurized;
    }

    public void setPasteurized(boolean pasteurized) {
        this.pasteurized = pasteurized;
    }

    public String toString() {
        return "Name: " + super.getName() + "\n" + super.toString() + "Pasteurized: " + this.pasteurized + "\n"
                + "-------------------------------";
    }

    public double total() {
        total = getKgs() * getPrice() * (1 + getTaxRate() / 100);
        return total;
    }
}
