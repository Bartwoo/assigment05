package week05;
import java.util.ArrayList;
import java.util.*;

public class Main {

    static double sk, pep, tab, jn, mk;

    public static void main(final String[] args) {

        ArrayList<Product> products = new ArrayList<Product>();

        products.add(new Skirt(5, "M&S", "red", 20, "skirt"));
        products.get(0).setPrice(40);

        Vegetable pepper = new Vegetable(18, 1, false, "pepper");
        pepper.setKgs(10);
        products.add(pepper);

        DishWashing tablet = new DishWashing("C");
        tablet.setLts(30);
        tablet.setPrice(25);
        tablet.setLiquid(false);
        tablet.setName("tablet");
        products.add(tablet);

        Trousers jean = new Trousers(30, "blue", "B");
        jean.setName("jean");
        jean.setPrice(50.99);
        jean.setQuantity(10);
        products.add(jean);

        Dairy milk = new Dairy(25, 1, true, "milk");
        milk.setExpirationDate("09.04.2017");
        milk.setPrice(2.95);
        products.add(milk);

        double total = 0;
        double rate = 20;

        products.get(0).setQuantity(5);
        sk = products.get(0).getPrice() * products.get(0).getQuantity() * (1 + rate / 100);
        pep = pepper.total();
        tab = tablet.total();
        jn = jean.total();
        mk = milk.total();

        //System.out.println(mk);

        Collections.swap(products, 1, 3);

        //System.out.println(products.get(1));

        for (final Product pp : products) {
            System.out.println(pp);
        }

        calculateTotalPrice(sk, pep, tab, jn, mk, total);
    }

    public static void calculateTotalPrice(double skirt, double pepper, double tablet, double jean, double milk,
            double total) {
        total = skirt + pepper + tablet + jean + milk;
        System.out.println("\tTotal is: " + total);
    }
}