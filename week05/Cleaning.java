package week05;
public class Cleaning extends Product {
    private String brand;

    public Cleaning(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String toString() {
        return super.toString() +
                "Brand: " + this.brand + "\n";
    }
}
