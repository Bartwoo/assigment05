package week05;
public class DishWashing extends Detergent {
    private double price, lts, total, rate = 18.0;

    public DishWashing(String brand) {
        super(brand);
    }    
    
    

    public double total() {
        total = getLts() * getPrice() * (1 + getTaxRate() / 100);
        return total;
    }
}
