package week05;
public class Product {
    private double taxRate;
    private double price,lts;
    private String name;
    private int quantity;

    public Product() {
        this.taxRate = 18;
        this.price = 1;
    }

    public Product(double taxRate) {
        this.taxRate = taxRate;
    }

    public Product(double taxRate, double price) {
        this.taxRate = taxRate;
        this.price = price;
    }    

    public double getTaxRate() {
        return this.taxRate;
    }

    

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "Tax rate: " + this.taxRate + "\n" +
                "Price: " + this.price + "\n";
    }
    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
