package week05;
public class Detergent extends Cleaning {
    private double lts;
    private boolean liquid;

    public Detergent(String brand) {
        super(brand);
    }

    public Detergent(String brand, double lts, boolean liquid) {
        super(brand);
        this.lts = lts;
        this.liquid = liquid;
    }    

    public boolean isLiquid() {
        return this.liquid;
    }

    public void setLiquid(boolean liquid) {
        this.liquid = liquid;
    }

    public void setLts(double lts) {
        this.lts = lts;
    }

    public double getLts(){
        return this.lts;
    }

    public String toString() {
        System.out.println("-------------------------------" + "\nPurchased Detergent Items");
        System.out.println();
        return "Name: " + super.getName()
         + "\n" + /* super.toString() */"Tax rate: "  + getTaxRate()+
         "\nPrice: "+ getPrice() + 
         "\nBrand: " + super.getBrand() +
          "\nLts: " + lts + "\n" + "Liquid: "
                + this.liquid;
    }
}
