package week05;
public class Trousers extends Cloth {
    private double kgs, price, taxRate, total;

    private int quantitiy;

    public Trousers(double taxRate, String color, String brand) {
        super(taxRate, color, brand);
    }  

    public String toString() {
        return "Name: " + super.getName() + "\n" + super.toString();
    }    

    public double total() {
        total = getQuantity() * getPrice() * (1 + getTaxRate() / 100);
        return total;
    }
}
