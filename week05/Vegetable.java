package week05;
public class Vegetable extends Food {
    private String name;
    private boolean liquid, organic;
    private double price, rate, total;
    private int kgs;    
    
    public Vegetable(double rate, double price, boolean liquid, String name) {
        super(name);
        this.price = price;
        this.rate = rate;
        this.liquid = liquid;
    }

    public boolean isOrganic() {
        return this.organic;
    }

    public void setOrganic(boolean organic) {
        this.organic = organic;
    }    

    public String toString() {
        System.out.println("-------------------------------" + "\nPurchased Food Items");
        System.out.println();
        return "Name: " + super.getName() +
         "\nTax rate: "+getTaxRate()+
         "\nPrice: " + getPrice()+
         "\nKgs: "+getKgs()+
         "\nExpiration date: "+"13.03.2017"+"\n";
    }

    public double total() {
        total = getKgs() * getPrice() * (1 + getTaxRate() / 100);
        return total;
    }
}
